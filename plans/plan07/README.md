# Scaling your application

## TODO

* Find the right place to scale the deployment.
* Do it then.
* See the result in the management console of the workshop leader.

## Result

* You have to have access to the management console (or kubernetes locally) to see the effect.
* So if you have managed that, get in touch with the workshop owner to have a look.

## Additional options

Have a look at the page [Exposing the deployment](https://cloud.google.com/kubernetes-engine/docs/quickstart#exposing_the_deployment) how to get an external IP to see the result of your deployment. Caveat: Not checked, if that really works ... :-)

Or do you want to include in your static web application something interesting or even useful? Think about what you have to change and what to do in deploying it? Why?
